
values = [{'M': 0, 'P': 1},
{'R': 0, 'E': 1},
{'N': 0, 'L': 1},
{'C': 0, 'I': 1},
{' ': 0, 'MP': 1},
{'RE': 0, 'NL': 1},
{'S': 0, 'O': 1},
{'CI': 0, ' MP': 1},
{'RENL': 0, 'SO': 1},
{'CI MP': 0, 'RENLSO': 1}]

def cleanNodes(l: list):
    nodes = []
    for value in l:
        nodes.append(tuple(value.keys()))
    nodes.reverse()
    return nodes


def buildTree(nodes: list) -> (dict, list):
    """
    @returns full tree as dictionary, list of tuples containing leaf nodes along with their numbers 
    tree structure: {PARENT: (LEFT_CHILD, RIGHT_CHILD, PARENT_NUMBER)}
    leaves: [(LEAF, LEAF_NUMBER)]
    """
    if not nodes or len(nodes) == 0:
        return None
    # 'prev' list structure:   ("NODE'S NAME", "NODE'S NUMBER IN STRING FORMAT") e.g ("CI", "00")
    
    prev = [("".join(nodes[0][0] + nodes[0][1]), "")] #Root, no number
    last_element_read = -1 
    tree = {}
    finished = False
    leaves = []
    while not finished:
        start_at = last_element_read + 1
        end_at = last_element_read + len(prev) + 1  
        
        tree_index = 0 # index of the 'prev' node we're looking at
        new_elements = []
        
        for i in range(start_at, end_at, 1):
            if (i == 0): # root node
                node_number = ""
            else:
                # 0 for left nodes, 1 for right nodes. Horrible way to do it, but it works
                node_number = str(int(tree_index%2==0)) 
            parent = prev[tree_index]
            # Store children and number. At this point I realized I should've used a class but fuck it
            tree[parent[0]] = (nodes[i][0], nodes[i][1], parent[1] + node_number)
            # Look at the children nodes FROM RIGHT TO LEFT and add them to new elements if they're
            # not leaves. Otherwise, add them to the tree
            for ch_index,  child_node in enumerate([nodes[i][1], nodes[i][0]]):
                if len(child_node) > 1:
                    new_elements.append((child_node, tree[parent[0]][2]))
                else:
                    node_number = tree[parent[0]][2] + str(int(ch_index == 0)) #I'm truly sorry
                    tree[child_node] = (node_number)
                    leaves.append((child_node, node_number))
            tree_index += 1
        last_element_read = end_at - 1
        prev = new_elements
        if (len(prev) == 0):
            finished = True
    return tree, leaves

tree, leaves = buildTree(cleanNodes(values))
print("Tree:",tree)
print("\n\nLeaves",leaves)