"""
Práctica 1. Código de Huffmann y Teorema de Shannon
"""
from myHuffman import crearArbol, distribucion
import os
import numpy as np
import pandas as pd

#### Vamos al directorio de trabajo
os.getcwd()
#os.chdir(ruta)
#files = os.listdir(ruta)

with open('GCOM2024_pract1_auxiliar_eng.txt', 'r',encoding="utf8") as file:
      en = file.read()
     
with open('GCOM2024_pract1_auxiliar_esp.txt', 'r',encoding="utf8") as file:
      es = file.read()

texto = 'COMPRESSION IS COOL'


#### Contamos cuantos caracteres hay en cada texto
from collections import Counter
tab_en = Counter(en)
tab_es = Counter(es)

#### Transformamos en formato array de los carácteres (states) y su frecuencia
#### Finalmente realizamos un DataFrame con Pandas y ordenamos con 'sort'
tab_en_states = np.array(list(tab_en)) #Crea una lista de todas las letras distintas que aparecen en orden de aparicion
tab_en_weights = np.array(list(tab_en.values())) #Crea una lista dando el valor de las apariciones de cada letra
tab_en_probab = tab_en_weights/float(np.sum(tab_en_weights)) #Regla Laplace para calcular la probabilidad
distr_en = pd.DataFrame({'states': tab_en_states, 'probab': tab_en_probab})
distr_en = distr_en.sort_values(by='probab', ascending=True) #Ordena los valores en funcion de su probabilidad de menor a mayor
distr_en.index=np.arange(0,len(tab_en_states)) 

#Análogo para la frase en Español
tab_es_states = np.array(list(tab_es))
tab_es_weights = np.array(list(tab_es.values()))
tab_es_probab = tab_es_weights/float(np.sum(tab_es_weights))
distr_es = pd.DataFrame({'states': tab_es_states, 'probab': tab_es_probab })
distr_es = distr_es.sort_values(by='probab', ascending=True)
distr_es.index=np.arange(0,len(tab_es_states))

##### Para obtener una rama, fusionamos los dos states con menor frecuencia
distr = distr_en
''.join(distr['states'][[0,1]])

### Es decir:
states = np.array(distr['states'])
probab = np.array(distr['probab'])
state_new = np.array([''.join(states[[0,1]])])   #Ojo con: state_new.ndim
probab_new = np.array([np.sum(probab[[0,1]])])   #Ojo con: probab_new.ndim
codigo = np.array([{states[0]: 0, states[1]: 1}])
states =  np.concatenate((states[np.arange(2,len(states))], state_new), axis=0)
probab =  np.concatenate((probab[np.arange(2,len(probab))], probab_new), axis=0)
distr = pd.DataFrame({'states': states, 'probab': probab, })
distr = distr.sort_values(by='probab', ascending=True)
distr.index=np.arange(0,len(states))

#Creamos un diccionario
branch = {'distr':distr, 'codigo':codigo}

def seleccion_idioma(idioma):
#Creamos una funcion para hacer basicamente lo que hicimos arriba con los idiomas en o es, esta funcion nos sera util
#para hacerlo de una forma mas general y para luego recorrer el arbol de forma inversa
    tab_idioma = Counter(idioma)
    tab_idioma_states = np.array(list(tab_idioma))
    tab_idioma_weights = np.array(list(tab_idioma.values()))
    tab_idioma_probab = tab_idioma_weights/float(np.sum(tab_idioma_weights))
    distr_idioma = pd.DataFrame({'states': tab_idioma_states, 'probab': tab_idioma_probab})
    distr_idioma = distr_idioma.sort_values(by='probab', ascending=True)
    distr_idioma.index = np.arange(0, len(tab_idioma_states))
    distr = distr_idioma
    ''.join(distr['states'][[0, 1]])
    return distr

distrTexto = seleccion_idioma(texto)

## Ahora definimos una función que haga exáctamente lo mismo
def huffman_branch(distr):
    states = np.array(distr['states'])
    probab = np.array(distr['probab'])
    state_new = np.array([''.join(states[[0,1]])])
    probab_new = np.array([np.sum(probab[[0,1]])])
    codigo = np.array([{states[0]: 0, states[1]: 1}])
    states =  np.concatenate((states[np.arange(2,len(states))], state_new), axis=0)
    probab =  np.concatenate((probab[np.arange(2,len(probab))], probab_new), axis=0)
    distr = pd.DataFrame({'states': states, 'probab': probab})
    distr = distr.sort_values(by='probab', ascending=True)
    distr.index=np.arange(0,len(states))
    branch = {'distr':distr, 'codigo':codigo}
    return(branch) 

def huffman_tree(distr):
    tree = np.array([])
    while len(distr) > 1:
        branch = huffman_branch(distr)
        distr = branch['distr']
        code = np.array([branch['codigo']])
        tree = np.concatenate((tree, code), axis=None)
    return(tree)
 
distr = distr_en 
tree = huffman_tree(distr)
tree[0].items()
tree[0].values()

#Buscar cada estado dentro de cada uno de los dos items
list(tree[0].items())[0][1] ## Esto proporciona un '0'
list(tree[0].items())[1][1] ## Esto proporciona un '1'


def diccionario_binario(idioma):
#Dado el idioma recorremos tree para asignar a cada caracter su numero binario correspondiente los cuales recogeremos en 
#un diccionario

    #distr =  seleccion_idioma(idioma)
    #tree = huffman_tree(distr)
    tree = crearArbol(distribucion(idioma))
    dic_binario = {} #Creamos sun diccionario vacio en el que añadiremos cada elemento con su valor binario

    for nodo in tree: #Recorremos cada nodo de tree(arbol de Huffman) y creamos dos listas con los valores que toman 0 o 1
        key_izq = list(nodo.keys())[0]
        key_dcha = list(nodo.keys())[1]

        if len(key_dcha) == 1: #Si la longitud es 1, significa que solo hay un caracter y con .get asignamos el valor correspondiente a dicho caracter
            dic_binario[key_dcha] = nodo.get(key_dcha)
        else:
            keys_chars = list(dic_binario.keys())
            for key_char in keys_chars:
                if key_char in key_dcha:
                    dic_binario[key_char] = str(nodo.get(key_dcha)) + str(dic_binario[key_char])

        if len(key_izq) == 1: #Análogo con el caso de key_dcha
            dic_binario[key_izq] = nodo.get(key_izq)
        else:
            keys_chars = list(dic_binario.keys())
            for key_char in keys_chars:
                if key_char in key_izq:
                    dic_binario[key_char] = str(nodo.get(key_izq)) + str(dic_binario[key_char])

    return dic_binario

import math
def longitud_media(idioma):
    dict_bin = diccionario_binario(idioma)
    distr = seleccion_idioma(idioma)
    v = list(dict_bin.values())
    k = list(dict_bin.keys())
    s = list(distr.states)
    prob = list(distr.probab)
    new = []
    new2 = []
    long = []
    for i in range(0, len(k)):
        for j in range(0, len(s)):
            if k[i] == s[j]:
                new.append(prob[j])
                
    for i in range(0, len(v)):
        long.append(float(len(v[i])))
    for i in range(0, len(v)):
        new2.append(new[i] * long[i])
    L = sum(new2)
    lista = []
    for i in range(0, len(prob)):
        lista.append(prob[i]*math.log2(prob[i]))
    H = -sum(lista)
    return L, H


def codificar(palabra, idioma):
    dict_bin =  diccionario_binario(idioma)
    codificado = ''
    for char in palabra:
        codificado = codificado + dict_bin[char]
    return codificado

file1 = open('GCOM2024_pract1_auxiliar_eng.txt', "r",encoding='utf8') 
content_en = str(file1.read())

file2 = open('GCOM2024_pract1_auxiliar_esp.txt', "r",encoding='utf8')
content_es = str(file2.read())

def decodificar(numeros,idioma):
    dict_bin =  diccionario_binario(idioma)
    size = len(numeros)
    binarios = ''
    decodificado = ''
    for i in range(0, size):
        binarios = binarios + str(numeros[i]) #Añadimos el primer numero (será o 0 o 1) 
        if binarios in dict_bin.values(): #Comprobamos si el numero binario coincide con algun caracter(en binario) en el diccionario
            decodificado = decodificado + \
                str(list(dict_bin.keys())[list(dict_bin.values()).index(binarios)])
            binarios = ''
    return decodificado
        

           
    