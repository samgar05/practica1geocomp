'''
Samuel Fernando Garcia Herrera 
Prac1. Implementacion del algoritmo de Huffman y comprobacion del teorema de Shannon
'''
import os
from math import log2
from collections import Counter

# PASO 0 - cargar el texto para uso posterior
os.getcwd()

file1 = open('GCOM2024_pract1_auxiliar_eng.txt', "r",encoding='utf8') 
content_en = str(file1.read())

file2 = open('GCOM2024_pract1_auxiliar_esp.txt', "r",encoding='utf8')
content_es = str(file2.read())

#PASO 1 Creamos una distribucion de frecuencias
def distribucion(texto:str) -> dict:
    '''
    @Toma un texto y da un diccionario con distribucion de probabilidades de cada caracter
    '''
    lenString = len(texto)
    dicFrec = Counter(texto)
    listaProbs = list(map(lambda x: x/lenString, list(dicFrec.values())))
    dicProb = dict(zip(list(dicFrec.keys()),listaProbs))
    dicProb = dict(sorted(dicProb.items(), key=lambda item: item[1]))
    return dicProb

#PASO 2 -- Arbol
def crearRama(dicProb:dict) -> dict:
    '''
    @devuelve un diccionario de la forma {Caracter: NumBinario, Caracter: NumBinario}
    determina la estructura de un nodo con sus dos hijos a la iz (0) o dr (1)
    '''
    rama = {}
    firstKey = list(dicProb.keys())[0]
    secondKey = list(dicProb.keys())[1]
    rama[firstKey] = 0
    rama[secondKey] = 1
    return rama

def crearArbol(dicProb:dict) -> list:
    '''
    @devuelve una lista con diccionarios de la forma dada por crearRama
    '''
    valores = []
    while len(dicProb) >= 2 :
        rama = crearRama(dicProb)
        firstKey = list(dicProb.keys())[0]
        secondKey = list(dicProb.keys())[1]
        union = firstKey + secondKey
        jointWeights = list(dicProb.values())[0] + list(dicProb.values())[1]
        dicProb[union] = jointWeights
        dicProb = dict(sorted(dicProb.items(), key=lambda item: item[1]))
        del dicProb[firstKey]
        del dicProb[secondKey]
        valores.append(rama)
    return valores


#PASO 3 - creamos el diccionario
def diccionarioHuffman(texto:str) -> dict:
    '''
    @devuelve un diccionario con las correspondecias de cada letra con su codificacion binaria
    Ej. {'S':'1010', 'E':'1110'}

    '''
    arbol = crearArbol(distribucion(texto))
    codigoHuff = {} 

    for nodo in arbol: 
        leftKey, rightKey = list(nodo.keys())[0], list(nodo.keys())[1]
    
        if len(leftKey) == 1: 
            codigoHuff[leftKey] = nodo[leftKey]
        else:
            letrasYaVistas = list(codigoHuff.keys())
            for letra in letrasYaVistas:
                if letra in leftKey:
                    codigoHuff[letra] = str(nodo[leftKey]) + str(codigoHuff[letra])
        
        if len(rightKey) == 1:
            codigoHuff[rightKey] = nodo[rightKey]
        else:
            letrasYaVistas = list(codigoHuff.keys())
            for letra in letrasYaVistas:
                if letra in rightKey:
                    codigoHuff[letra] = str(nodo[rightKey]) + str(codigoHuff[letra]) 
                    # sumando de derecha a izquierda
    return codigoHuff

#PASO 4 - codificacion y decodificacion de texto
def codificar(texto:str, codigoHuff:dict) -> str: 
    #codifica en terminos del diccionario dado
    codigo = ""
    for letra in texto:
        codigo += codigoHuff[letra]
    return codigo

def decodificar(binario:str, codigoHuff:dict) -> str:
    decodigo = ""
    decodigoHuff = {y: x for x, y in codigoHuff.items()}
    currentString = ""
    for bit in binario:
        currentString += bit
        if currentString in codigoHuff.values():
            decodigo += decodigoHuff[currentString]
            currentString = ""
    return decodigo

#PASO 5 - Comprobamos Teorema de Shannon calculando la Entropia y la longitud media
def H(texto:str) -> float: #Entropia
    dicProb = distribucion(texto)
    total = 0
    for key in dicProb.keys():
        prob = dicProb[key]
        total += -prob*log2(prob)
    return total

def L(texto:str) -> float: #longitud media
    dicProb = distribucion(texto)
    codigoHuff = diccionarioHuffman(texto)
    dicLongitudes = dict(zip(codigoHuff.keys(),list(map(lambda x: len(x), codigoHuff.values()))))
    total = 0
    for key in dicLongitudes.keys():
        prob = dicProb[key]
        longitud = dicLongitudes[key]
        total += prob*longitud
    return total

def comprobarShannon(texto:str) -> bool:
    return (H(texto) <= L(texto)) and (L(texto) <= H(texto) +1)

if __name__ == '__main__':
    dicHuffES = diccionarioHuffman(content_es)
    dicHuffEN = diccionarioHuffman(content_en)
    print('Codificacion de la palabra "Lorentz" con el diccionario en espanol: {}'.format(codificar('Lorentz', dicHuffES)))
    print('Codificacion de la palabra "Lorentz" con el diccionario en ingles: {}\n\n'.format(codificar('Lorentz', dicHuffEN)))
    print('Entropia del texto en espanol: {}\nLongitud Media del texto en espanol: {}\n'.format(H(content_es),L(content_es)))
    print('Entropia del texto en ingless: {}\nLongitud Media del texto en ingles: {}'.format(H(content_en),L(content_en)))
    